import logging

from django.core import mail

from machado.apps.core import facade

logger = logging.Logger(__file__)


def cadastrar_novo_mapa(titulo: str, dono=None, dono_id=None):
    mapa = facade.persistir_mapa(titulo, dono, dono_id)
    mail.send_mail(
        'Novo mapa cadastrado',
        f'confira o mapa com id  {mapa.id}',
        'renzo@python.pro.br',
        ['luizvidal@necto.com.br'],
    )
    logger.info(f'Mapa criado {mapa.id}')
