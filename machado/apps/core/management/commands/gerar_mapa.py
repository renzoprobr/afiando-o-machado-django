from django.core.management import BaseCommand

from machado.apps.core import facade
from machado.apps.core.facade import MapaPersistenciaErro


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--donoid', help='identificador do dono do mapa')
        parser.add_argument('--titulo', help='titulo do mapa')
        super().add_arguments(parser)

    def handle(self, *args, **options):
        dono_id = int(options['donoid'])
        titulo = options['titulo']
        try:
            facade.persistir_mapa(titulo, dono_id=dono_id)
        except MapaPersistenciaErro as erro:
            for campo, erros in erro.form.errors.items():
                print(f'Erro(s) do campo {campo}:')
                for erro in erros:
                    print(f'    {erro}')
