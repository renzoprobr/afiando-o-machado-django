from machado.apps.core.forms import MapaForm


class MapaPersistenciaErro(Exception):

    def __init__(self, form, *args: object) -> None:
        super().__init__(*args)
        self.form = form


def persistir_mapa(titulo: str, dono=None, dono_id=None):
    """
    Função persiste um mapa.
    deve ser passado dono ou dono_id

    :param titulo:
    :param dono:
    :param dono_id:
    :return:
    """
    if dono is not None:
        dono_id = dono.id
    form = MapaForm({'titulo': titulo}, dono_id=dono_id)
    if not form.is_valid():
        raise MapaPersistenciaErro(form)
    return form.save()
