import json

from django import forms
from django.forms.utils import ErrorList

from machado.apps.core.models import Mapa, Cadastro


class MapaForm(forms.ModelForm):

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None, use_required_attribute=None, renderer=None,
                 dono_id=None):
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance,
                         use_required_attribute, renderer)

        self.dono_id = dono_id

    def save(self, commit=True):
        mapa = super().save(commit=False)
        mapa.dono_id = self.dono_id
        if commit:
            mapa.save()
        return mapa

    class Meta:
        model = Mapa
        fields = ('titulo',)


class JsonFormMixin:
    def as_json(self):
        dct = {name: field.to_python(self[name].data) for name, field in self.fields.items()}
        return json.dumps(dct)


class CadastroForm(JsonFormMixin, forms.ModelForm):
    class Meta:
        model = Cadastro
        exclude = []

    hobbies = forms.MultipleChoiceField(choices=Cadastro.Hobby.choices)
