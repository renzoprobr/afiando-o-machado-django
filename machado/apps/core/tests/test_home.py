# Create your tests here.
from django.urls import reverse


def test_home_status(client):
    response = client.get(reverse('core:home'))
    assert 200 == response.status_code
