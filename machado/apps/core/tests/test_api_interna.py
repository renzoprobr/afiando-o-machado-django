# Create your tests here.
import pytest
from django.urls import reverse
from django.utils.datetime_safe import datetime


def test_api_status(client):
    response = client.get(reverse('core:api_interna'))
    assert 200 == response.status_code


@pytest.mark.freeze_time(datetime(2021, 3, 24, 1, 2, 3))
def test_api_msg(client):
    response = client.get(reverse('core:api_interna'))
    assert {
               'msg': 'Hello World',
               'today': '2021-03-24',
               'now': '2021-03-24 01:02:03'
           } == response.json()
