# Create your tests here.
import pytest
from django.urls import reverse

from pytest_django.asserts import assertTemplateUsed, assertContains


@pytest.fixture
def response_get(client, responses):
    responses.add(responses.GET, 'https://api.github.com/users/renzon', json=avatar_response)
    yield client.get(reverse('core:github', kwargs={'username': 'renzon'}))


def test_github_status(response_get):
    assert 200 == response_get.status_code


def test_template_utilizado(response_get):
    assertTemplateUsed(response_get, 'core/github.html')


def test_avatar_utilizado(response_get):
    assertContains(
        response_get,
        'https://avatars.githubusercontent.com/u/3457115?v=4'
    )


avatar_response = {
    "login": "renzon",
    "id": 3457115,
    "node_id": "MDQ6VXNlcjM0NTcxMTU=",
    "avatar_url": "https://avatars.githubusercontent.com/u/3457115?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/renzon",
    "html_url": "https://github.com/renzon",
    "followers_url": "https://api.github.com/users/renzon/followers",
    "following_url": "https://api.github.com/users/renzon/following{/other_user}",
    "gists_url": "https://api.github.com/users/renzon/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/renzon/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/renzon/subscriptions",
    "organizations_url": "https://api.github.com/users/renzon/orgs",
    "repos_url": "https://api.github.com/users/renzon/repos",
    "events_url": "https://api.github.com/users/renzon/events{/privacy}",
    "received_events_url": "https://api.github.com/users/renzon/received_events",
    "type": "User",
    "site_admin": False,
    "name": "Renzo Nuccitelli",
    "company": "Python Pro",
    "blog": "https://www.python.pro.br",
    "location": "Brazil",
    "email": None,
    "hireable": True,
    "bio": None,
    "twitter_username": None,
    "public_repos": 170,
    "public_gists": 64,
    "followers": 623,
    "following": 3,
    "created_at": "2013-02-02T14:15:53Z",
    "updated_at": "2021-03-21T12:58:12Z"
}
