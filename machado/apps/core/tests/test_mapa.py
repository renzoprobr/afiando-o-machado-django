# Create your tests here.
import pytest
from django.urls import reverse
from model_bakery import baker
from pytest_django.asserts import assertTemplateUsed, assertContains

from machado.apps.core.models import Mapa


@pytest.fixture
def response_get(client_with_logged_user):
    return client_with_logged_user.get(reverse('core:mapa'))


def test_http_status(response_get):
    assert 200 == response_get.status_code


def test_template_utilizado(response_get):
    assertTemplateUsed(response_get, 'mapa/mapa.html')


def test_form_utilizado(response_get):
    assertContains(response_get, '<form')
    assertContains(response_get, 'name="titulo"')


@pytest.fixture
def response_post(client_with_logged_user, db):
    return client_with_logged_user.post(reverse('core:mapa'), {'titulo': 'São Paulo'})


def test_create(response_post):
    assert Mapa.objects.count() == 1


def test_propriedades_do_formulario(response_post):
    mapa = Mapa.objects.first()
    assert 'São Paulo' == mapa.titulo


def test_dono_do_mapa(response_post, logged_user):
    mapa = Mapa.objects.first()
    assert logged_user == mapa.dono


@pytest.fixture
def mapa(logged_user):
    mapa = baker.make(Mapa, dono=logged_user)
    return mapa


def test_delete(mapa, client_with_logged_user):
    client_with_logged_user.post(reverse('core:mapa_apagar'), data={'mapa_id': str(mapa.id)})
    assert not Mapa.objects.exists()


@pytest.fixture
def mapas(db, logged_user):
    return baker.make(Mapa, dono=logged_user, _quantity=2)


@pytest.fixture
def response_api(client, mapas):
    return _response_api(client)


def _response_api(client):
    return client.get(reverse('core:api_mapas'))


def test_api_list(response_api, logged_user, mapas):
    assert {
               'mapas': [
                   {'mapa_titulo': mapa.titulo, 'dono_email': logged_user.email} for mapa in mapas
               ]
           } == response_api.json()


def test_api_list_num_queries(mapas, client, django_assert_max_num_queries):
    with django_assert_max_num_queries(1):
        _response_api(client)
