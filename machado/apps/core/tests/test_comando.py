from django.core.management import call_command

from machado.apps.core.models import Mapa


def test_gerar_mapa_comando(logged_user):
    call_command('gerar_mapa', donoid=str(logged_user.id), titulo='Meu Mapa')
    assert Mapa.objects.exists()
    mapa = Mapa.objects.first()
    assert mapa.dono_id == logged_user.id
    assert mapa.titulo == 'Meu Mapa'


def test_titulo_muito_grande(logged_user):
    assert not Mapa.objects.exists()
    call_command('gerar_mapa', donoid=str(logged_user.id), titulo='a' * 65)
    assert not Mapa.objects.exists()
