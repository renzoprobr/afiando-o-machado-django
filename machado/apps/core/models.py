from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.db import models


class Mapa(models.Model):
    titulo = models.CharField(max_length=64)
    dono = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)


class Cadastro(models.Model):
    nome = models.CharField(verbose_name='NoMe', max_length=64)
    email = models.EmailField()
    idade = models.IntegerField()
    sexo = models.CharField(
        max_length=1,
        choices=[('M', 'Masculino'), ('F', 'Feminino'), ('O', 'Outros')]
    )

    class Hobby(models.TextChoices):
        FUTEBOL = ('futebol', 'Futebol')
        GAME = ('game', 'Game')
        LEITURA = ('leitura', 'Leitura')

    hobbies = ArrayField(models.CharField(max_length=64, choices=Hobby.choices))
