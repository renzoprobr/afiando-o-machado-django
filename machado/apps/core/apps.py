from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'machado.apps.core'
