import requests
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
# Create your views here.
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils import timezone

from machado.apps.core.facade import MapaPersistenciaErro
from machado.apps.core.forms import MapaForm, CadastroForm
from machado.apps.core.models import Mapa
from machado.domain import mapa_dominio


def home(request):
    if request.method == 'POST':
        form = CadastroForm(request.POST)

        if not form.is_valid():
            return render(request, 'core/index.html', {'form': form})
        # Salvar dados
        form.save()

        # Abre a transação
        # Salva Produtos ou use o que já existe
        # Salva Organização ou use o que já existe
        # Salva todos modelos intemediários com fk de produto e de organização
        # Encerra transação

        return redirect('https://google.com')
    form = CadastroForm()
    return render(request, 'core/index.html', {'form': form})


def github(request, username):
    context = {
        'avatar_url': requests.get(f'https://api.github.com/users/{username}').json()['avatar_url']
    }
    return render(request, 'core/github.html', context)


@login_required
def mapa(request):
    if request.method == 'POST':
        try:
            mapa_dominio.cadastrar_novo_mapa(request.POST['titulo'], dono=request.user)
        except MapaPersistenciaErro as erro:
            return render(request, 'mapa/mapa.html', context={'form': erro.form}, status=401)
        return redirect(reverse('core:home'))

    return render(request, 'mapa/mapa.html', context={'form': MapaForm()})


@login_required
def mapa_apagar(request):
    if request.method == 'POST':
        mapa_id = int(request.POST['mapa_id'])
        Mapa.objects.filter(id=mapa_id).delete()
        return HttpResponse('Apagado')


def api_interna(request):
    now = timezone.now()
    return JsonResponse(
        {
            'msg': 'Hello World',
            'today': now.strftime('%Y-%m-%d'),
            'now': now.strftime('%Y-%m-%d %H:%M:%S'),
        }
    )


def api_mapas(request):
    mapas = Mapa.objects.order_by('id').select_related('dono').only('titulo', 'dono__email')
    return JsonResponse(
        {
            'mapas': [{'mapa_titulo': mapa.titulo, 'dono_email': mapa.dono.email} for mapa in mapas]
        }
    )
