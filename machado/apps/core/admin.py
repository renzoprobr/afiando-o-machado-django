from django.contrib import admin

from machado.apps.core.models import Mapa, Cadastro


@admin.register(Mapa)
class MapaAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'dono')


@admin.register(Cadastro)
class CadastroAdmin(admin.ModelAdmin):
    list_display = ('nome', 'email', 'idade', 'sexo', 'hobbies')
