import pytest
from django.contrib.auth import get_user_model
from model_bakery import baker


@pytest.fixture
def logged_user(db):
    User = get_user_model()
    return baker.make(User)


@pytest.fixture
def client_with_logged_user(logged_user, client):
    client.force_login(logged_user)
    return client
