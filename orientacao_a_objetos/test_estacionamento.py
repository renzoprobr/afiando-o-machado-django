"""

    Parte não tão fácil

    Agora o estaciamento aceita motos. Só que uma moto ocupa meia vaga. Ou seja, em uma vaga, cabem duas motos

    >>> from pprint import pprint
    >>> vaga = Vaga()
    >>> vaga
    Vazia()
    >>> repr(vaga)
    'Vazia()'
    >>> str(vaga)
    'vazia'
    >>> gol = Carro('gol')
    >>> vaga.estacionar(gol)
    >>> str(vaga)
    "Carro('gol')"
    >>> outra_vaga = Vaga()
    >>> cg= Moto('CG')
    >>> dt= Moto('DT')
    >>> outra_vaga.estacionar(cg)
    >>> outra_vaga.estacionar(dt)
    >>> str(outra_vaga)
    "[Moto('CG'), Moto('DT')]"
    >>> vaga.estacionar(Carro('santana'))
    Traceback (most recent call last):
      ...
    test_estacionamento.ImpossivelEstacionar: Carro('santana')
    >>> estacionamento = Estacionamento(andares=3, vagas_por_andar=4)
    >>> pprint(estacionamento.estacionar(gol))
    [["Carro('gol')", 'vazia', 'vazia', 'vazia'],
     ['vazia', 'vazia', 'vazia', 'vazia'],
     ['vazia', 'vazia', 'vazia', 'vazia']]
    >>> pprint(estacionamento.estacionar(Moto('cg')))
    [["Carro('gol')", "Moto('cg')", 'vazia', 'vazia'],
     ['vazia', 'vazia', 'vazia', 'vazia'],
     ['vazia', 'vazia', 'vazia', 'vazia']]
    >>> pprint(estacionamento.estacionar(Carro('santana')))
    [["Carro('gol')", "Moto('cg')", "Carro('santana')", 'vazia'],
     ['vazia', 'vazia', 'vazia', 'vazia'],
     ['vazia', 'vazia', 'vazia', 'vazia']]
    >>> pprint(estacionamento.estacionar(Moto('dt')))
    [["Carro('gol')", "[Moto('cg'), Moto('dt')]", "Carro('santana')", 'vazia'],
     ['vazia', 'vazia', 'vazia', 'vazia'],
     ['vazia', 'vazia', 'vazia', 'vazia']]

"""
from builtins import NotImplementedError
from typing import List


class Estacionamento:
    def __init__(self, andares=3, vagas_por_andar=4):
        pass
        # self._andares = [['vazia'] * vagas_por_andar for _ in range(andares)]
        # self._quantidade_carros_estacionados = 0

    def estacionar(self, *carros):
        pass

    def _estacionar_um_carro(self, carro):
        pass


class Veiculo:
    @property
    def slots_ocupados_em_vaga(self):
        raise NotImplementedError()

    def __init__(self, marca):
        self.marca = marca


class ImpossivelEstacionar(Exception):
    pass


class Vaga:
    QUANTIDADE_SLOTS = 2

    def __init__(self):
        self._veiculos: List[Veiculo] = []

    def __repr__(self):
        return 'Vazia()'

    def __str__(self):
        if len(self._veiculos) == 0:
            return 'vazia'
        elif len(self._veiculos) == 1:
            return repr(self._veiculos[0])
        return repr(self._veiculos)

    def estacionar(self, veiculo: Veiculo):
        slots_disponiveis = Vaga.QUANTIDADE_SLOTS - sum(
            veiculo_estacionado.slots_ocupados_em_vaga for veiculo_estacionado in self._veiculos)
        if slots_disponiveis < veiculo.slots_ocupados_em_vaga:
            raise ImpossivelEstacionar(veiculo)
        self._veiculos.append(veiculo)


class Carro(Veiculo):
    @property
    def slots_ocupados_em_vaga(self):
        return 2

    def __repr__(self):
        return f"Carro('{self.marca}')"


class Moto(Veiculo):
    @property
    def slots_ocupados_em_vaga(self):
        return 1

    def __repr__(self):
        return f"Moto('{self.marca}')"
