"""
Vamos montar um carrinho de ecommerce.

Ele deve permitir adição de produtos com respectiva quantidade

Deve possuir dois tipos de promoção:

1. Promoção por preço. Se o valor total da compra passar de R$ 1000,00, aplicar 10% de de desconto.

2. Promoção por quantidade. Se for comprado mais de 10 items de um produto, é dado 15% de desconto
no valor desses itens.

As promoções 1 e 2 não são cumulativas e o sistema escolhe automaticamente a melhor para o usuário
    >>> from decimal import Decimal as D
    >>> calcular_desconto(D('40159.99'), '0.10' )
    Decimal('4016.00')
    >>> carrinho = Carrinho()
    >>> carrinho.total()
    'R$ 0.00'
    >>> carrinho.desconto()
    'Sem desconto: R$ 0.00'
    >>> desconto_15_porcento_por_quantidade = DescontoPorQuantidade( D('0.15'), 10)
    >>> desconto_10_porcento_por_valor = DescontoPorValor(Decimal('0.10'), Decimal('1000.00'))
    >>> carrinho.adicionar_desconto(desconto_10_porcento_por_valor, desconto_15_porcento_por_quantidade)
    >>> carrinho.itens()
    []
    >>> camisa = Item('Camisa', D('59.99'))
    >>> carrinho.adicionar(camisa, 1)
    >>> carrinho.itens()
    [('Camisa', 1, 'R$ 59.99')]
    >>> carrinho.total()
    'R$ 59.99'
    >>> carrinho.desconto()
    'Sem desconto: R$ 0.00'
    >>> meia=Item('Meia', D('10.00'))
    >>> carrinho.adicionar(meia, 10)
    >>> carrinho.itens()
    [('Camisa', 1, 'R$ 59.99'), ('Meia', 10, 'R$ 100.00')]
    >>> carrinho.total()
    'R$ 159.99'
    >>> carrinho.desconto()
    'Desconto por quantidade, 15%: R$ 15.00'
    >>> carrinho.valor_a_pagar()
    'R$ 144.99'
    >>> carro = Item('Celta', D('40000.00'))
    >>> carrinho.adicionar(carro, 1)
    >>> carrinho.itens()
    [('Camisa', 1, 'R$ 59.99'), ('Meia', 10, 'R$ 100.00'), ('Celta', 1, 'R$ 40000.00')]
    >>> carrinho.total()
    'R$ 40159.99'
    >>> carrinho.desconto()
    'Desconto por valor, 10%: R$ 4016.00'
    >>> carrinho.valor_a_pagar()
    'R$ 36143.99'
    >>> civic=Item('Civic', D('100000.00'))
    >>> carrinho.adicionar(civic, 10)
    >>> carrinho.itens()
    [('Camisa', 1, 'R$ 59.99'), ('Meia', 10, 'R$ 100.00'), ('Celta', 1, 'R$ 40000.00'), ('Civic', 10, 'R$ 1000000.00')]
    >>> carrinho.total()
    'R$ 1040159.99'
    >>> carrinho.desconto()
    'Desconto por quantidade, 15%: R$ 150015.00'
    >>> carrinho.valor_a_pagar()
    'R$ 890144.99'
"""
from decimal import Decimal
from typing import Tuple, List


class Item:
    def __init__(self, nome: str, valor: Decimal):
        self.nome = nome
        self.valor = valor

    def __repr__(self):
        return repr(self.nome)


class Produto:
    def __init__(self, item: Item, quantidade: int):
        self.item = item
        self.quantidade = quantidade

    def __repr__(self):
        return repr((self.item, self.quantidade, f'R$ {self.subtotal}'))

    @property
    def subtotal(self):
        return self.item.valor * self.quantidade

    @property
    def desconto(self):
        if self.quantidade >= 10:
            return


def calcular_desconto(total_decimal, porcentagem_do_desconto):
    valor_do_desconto = total_decimal * Decimal(porcentagem_do_desconto)
    valor_do_desconto = round(valor_do_desconto, 2)
    return valor_do_desconto


class Desconto:
    def calcular(self, carrinho: 'Carrinho') -> Decimal:
        raise NotImplementedError()


class DescontoPorQuantidade(Desconto):
    def __init__(self, porcentagem_do_desconto: Decimal, quantidade_minima: int):
        super().__init__()
        self.porcentagem_do_desconto = porcentagem_do_desconto
        self.quantidade_minima = quantidade_minima

    def calcular(self, carrinho: 'Carrinho') -> Decimal:
        valor_desconto = Decimal('0.00')
        for produto in carrinho.produtos:  # esse self é o carrinho

            if produto.quantidade >= self.quantidade_minima:
                valor_desconto += calcular_desconto(
                    produto.subtotal, self.porcentagem_do_desconto
                )
        return valor_desconto

    def __str__(self):
        return f'Desconto por quantidade, {self.porcentagem_do_desconto:%}'


class DescontoPorValor(Desconto):
    def __init__(self, porcentagem_do_desconto: Decimal, valor_minimo_para_aplicar_desconto: Decimal):
        super().__init__()
        self.valor_minimo_para_aplicar_desconto = valor_minimo_para_aplicar_desconto
        self.porcentagem_do_desconto = porcentagem_do_desconto

    def calcular(self, carrinho: 'Carrinho') -> Decimal:
        total_decimal = carrinho._total_decimal()  # esse self é o carrinho
        valor_desconto = Decimal('0.00')
        if total_decimal >= self.valor_minimo_para_aplicar_desconto:
            valor_desconto = calcular_desconto(
                total_decimal, self.porcentagem_do_desconto
            )
        return valor_desconto

    def __str__(self):
        return f'Desconto por valor, {self.porcentagem_do_desconto:%}'


class _SemDesconto(Desconto):
    def calcular(self, carrinho: 'Carrinho') -> Decimal:
        return Decimal('0.00')

    def __str__(self):
        return 'Sem desconto'


SEM_DESCONTO = _SemDesconto()


class Carrinho:

    def __init__(self):
        self._descontos: List[Desconto] = [SEM_DESCONTO]
        self.produtos = []

    def itens(self):
        return self.produtos

    def total(self):
        valor_total = self._total_decimal()
        return f'R$ {valor_total}'

    def _total_decimal(self):
        valor_total = sum((produto.subtotal for produto in self.produtos), Decimal('0.00'))
        return valor_total

    def desconto(self):
        desconto, valor_do_desconto = self._escolher_melhor_desconto_e_valor()
        return f'{desconto}: R$ {valor_do_desconto}'

    def _escolher_melhor_desconto_e_valor(self):
        valor, _, desconto = max((d.calcular(self), -i, d) for i, d in enumerate(self._descontos))
        return desconto, valor

    def adicionar(self, item: Item, quantidade: int):
        self.produtos.append(Produto(item, quantidade))

    def valor_a_pagar(self):
        _, valor = self._escolher_melhor_desconto_e_valor()
        valor = self._total_decimal() - valor
        return f'R$ {valor}'

    def adicionar_desconto(self, *descontos: Tuple[Desconto]):
        self._descontos += descontos
