# afiando-o-machado-django

Projeto para construir um projeto Django com ferramentas fundamentais

## Como escrever mensagens de commit

https://git.znc.com.br/necto/necto-welcome/-/blob/master/source/necto_git/202002_git_issue.rst

Essa política foi inspirada nesse link:

https://github.com/erlang/otp/wiki/writing-good-commit-messages

Video do Renzo sobre histórico Linear

https://www.youtube.com/watch?v=GEr4g15LOqg
https://www.youtube.com/watch?v=usxQG-hOsDw

## Instalando dependências para desenvolvimento

Crie um virtualenv e instale as dependências de desenvolvimento:

```bash
pip install -r requirements-dev.txt
```
## Dependências

Para inserir uma dependência de produção, acrescente-a no arquivo [requirements.in](requirements.in).
Para inserir uma dependência de desenvolvimento, acrescente-a no arquivo [requirements-dev.in](requirements-dev.in)

NÃO FIXE AS VERSÕES NESSE ARQUIVO

Para gerar os arquivos de requirements use o comando:

```bash
pip-compile requirements.in --generate-hashes && pip-compile requirements-dev.in --generate-hashes 
```

Para sincronizar as dependências do seu vitualenv de desenvolvimento:

```bash
pip-sync requirements-dev.txt
```
## Linter

O flake8 é utilizado para linter.

Rode o comando para validar o código:
```bash
flake8 .
```

## Testes

Para rodar os testes:

```bash
pytest
``` 

Para rodar os testes em paralelo e com cobertura:
```bash
pytest . -n auto --cov=machado --cov-fail-under=74 --cov-report=term --cov-report html
```


